## What is features2?

Forking project [features2html]( https://github.com/melke/).

features2 is a simple script that **creates HTML and PDF based documentation from Cucumber features**. 
Note that the documentation is **generated from the source Cucumber feature files, and NOT from the test results** (there
are plenty of other tools that can do that).

Use features2 when you want to **create a nice looking requirements specification**, that you can email to your customer.
You can focus on editing the actual feature files and let features2 make the features presentable to your customers.

---

## Installation

* Make sure you have node.js installed on the computer where you intend to run the script.
* Install
   
  ```

        npm i -g features2 
        
  ```

* Or access npm [site](https://www.npmjs.com/package/features2). 

---

## Usage

### Demo usage

```


features2 create -i "path-to-your-feature-file-folder" -o "path-output-files" -l "language used in feature files" -p "Your Product Name" -d  "Your Product Description"


```

### Real world usage

Have a look at the switches available:

```
-a, --author [string]               The author name used in header (default: John Doe)
-b, --break-before-word [string]    Create a line break before every occurrence of this word in the background (default: null)
-d, --product-description [string]  The description of your product used in header (default: My Product Description)')
-g, --generate-pdf [false|true]     Is generate PDF file(default: true)
-h, --help                          Output usage information
-i, --input-dir [path]              Read feature files from path. This is required.
-l, --lang [pt|en|string]           Language used in feature files (default: pt, support all languagens gherkin)
-o, --output-file [path]            Send output to file relative path. This is required.
-p, --product-name [string]         The name of your product used in headline and header (default: My Product Name)
-t, --templates-dir [path]          Read the files doc_template.html, feature_template.html and style.css from path (default: default/templates)
-V, --version                       Output the version number
-x, --debug                         Output extra debugging

```

## Feedback and Contribution

All feedback and contribution is very appreciated. Please send pull requests or just send an email to [dedezanol9@gmail.com](mailto:dedezanol9@gmail.com).

---

## Licensing

feature2 is licensed under MIT License
Copyright 2020 features2

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

---