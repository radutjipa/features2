module.exports = {
    // DEFAULT SETTINGS
    FILE_ENCODING: 'utf-8',
    INPUTDIR: '/examples/features',
    TEMPLATESDIR: '/default/templates',
    PRODUCTNAME: 'My Product Name',
    PRODUCTDESCRIPTION: 'My Product Description',
    AUTHOR: 'Usuário',
    OUTPUTFILE: 'target',
    GENERATEPDF: true,
    LANGUAGE: 'pt',
    BREAKBEFOREWORD: null,
    DOCTEMPLATE: '',
    FEATURETEMPLATE: '',
}
